//create a constructor to create an object for the high score list
function uScore (name,score){
    this.userName=name;
    this.userScore=score;
}

//this is the array of scores for the list of high scores from previous time the game has been played
//has hardcoded scores so that the user can see how they did
var highScore = [{userName:"user1",userScore:50},{userName:"user2",userScore:90},{userName:"user3",userScore:0},
    {userName:"user4", userScore:20}, {userName:"user5",userScore:60}];
