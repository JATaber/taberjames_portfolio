//ask the user for the name they would like to use
var uName = prompt("Please enter the name you would like to use.");


//display the information acquired in the console
console.log(uName);

//create a variable to hold what the user selects
var input;

//create a variable to calculate the users score later on in the code
var currScore;

//use a do/while loop to run code according to what the user selected
do {
//create a menu for the user to select an option
    input = prompt("Please select the option number for what you would like to do?\r\n" +
        "1. Play Game\r\n" +
        "2. Display High Score.\r\n" +
        "3. Change username.\r\n" +
        "4. Exit");

//create a switch statement to run what the user selected to do
    switch (input) {
        case "1":
            //generate random number between 0 and 50 for the game
            var random = Math.floor(Math.random()*51);
            console.log("generated number");

            //make a while loop to check if the user has chances left (they get 10 to easily calculate a score
            var choices = 10;

            do{

                //make a variable to capture the users guess and let the user know how many choices are left
                var guess = prompt("Pick a number between 0 and 50");
                console.log(guess);

                //confirm that the user has entered a number with a while loop
                while (isNaN(Number(guess))) {
                    guess = prompt("It seems you didn't enter a number. Please try again.\r\n" +
                        "Pick a number between 0 and 50.");
                }

                //compare guess to the number generated and give correct feedback
                if (Number(guess) > random && choices >0) {
                     alert("Your guess of " + guess + " was to high.\r\n" +
                        "Please select a number lower than " + guess + ".");

                    //take a guess away from the user
                    choices--;
                    console.log(choices);

                } else if (Number(guess) < random && choices >0) {
                    alert("Your guess of " + guess + " was to low.\r\n" +
                        "Please select a number higher than " + guess + ".");

                    //take a guess away from the user
                    choices--;
                    console.log(choices);

                } else {
                    alert("CONGRATULATIONS!!\r\n" +
                        "YOU GUESSED IT CORRECTLY!!");
                    //let the user know they guessed the number correctly

                    console.log("guessed correct");
                }

            }
            while(choices > 0 && Number(guess) != random);

            //figure out user score from the amount of choices left
            currScore = (choices/10)*100;
            console.log(currScore);

            //push the users name and score into the array of high scores
            highScore.push({userName:uName, userScore: currScore});

            //checks the username of the newest entry into the array
            console.log(highScore[highScore.length-1].userName);

            //let the user know how they did
            alert("You finished with "+choices+" choices left.\r\n" +
                "Giving you a score of "+currScore+".");
            console.log("gave feedback to user");

            break;

        case "2":
            //sort the array of high scores in descending order by score
            highScore.sort(function(a,b) {return b.userScore - a.userScore});

            console.log("sorted");

            //display the sorted high scores to the user
            alert("-----Top 5 scores-----\r\n" +
                "1. "+highScore[0].userName+"---score: "+highScore[0].userScore+
                "\r\n2. "+highScore[1].userName+"---score: "+highScore[1].userScore+
                "\r\n3. "+highScore[2].userName+"---score: "+highScore[2].userScore+
                "\r\n4. "+highScore[3].userName+"---score: "+highScore[3].userScore+
                "\r\n5. "+highScore[4].userName+"---score: "+highScore[4].userScore);
            break;

        case "3":
            //ask the user for their new username
            uName = prompt("Please enter your new user name.");

            //print to the console to confirm new username
            console.log(uName);
            break;

        case "4":
            //print to the console what the application is exiting
            console.log("exited menu");
            break;

        default:
            //let the user know that they input an invaild selection and display the options over again
            alert("Invalid selection!\r\nPlease try again!");
            break;

    }
    //this will end the loop when the user selects option 4 which exits the menu
}while (input != 4);

console.log("code ends");