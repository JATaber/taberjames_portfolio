﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taber_James_Portfolio
{
    class SwapName
    {
       public string mFirstName;
        public string mLastName;

        public SwapName(string _firstName, string _lastName)
        {
            mFirstName = _firstName;
            mLastName = _lastName;
        }

        public string GetFirst()
        {
            return mFirstName;
        }

        public string GetLast()
        {
            return mLastName;
        }

        public void SetFirst(string _firstName)
        {
            this.mFirstName = _firstName;
        }

        public void SetLast(string _lastName)
        {
            this.mLastName = _lastName;
        }

        public string NameSwap()
        {
            string swapFirstName = mLastName;
            string swapLastName = mFirstName;
            return mLastName + mFirstName;
        }
    }
}
