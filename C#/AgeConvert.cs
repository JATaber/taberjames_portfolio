﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taber_James_Portfolio
{
    class AgeConvert
    {
        //Creating global variable
       public double mStartAge;
        public double mAgeDays;
        public double mAgeHours;
        public double mAgeMinutes;

        public AgeConvert(double _startAge)
        {
            mStartAge = _startAge;         
        }

        public double GetAge()
        {
            return mStartAge;
        }

        public void SetAge(double _startAge)
        {
            this.mStartAge = _startAge;
        }

        public double GetDays()
        {
            return mAgeDays;
        }

        public void SetDays(double _ageDays)
        {
            this.mAgeDays = _ageDays;
        }

        public double GetHours()
        {
            return mAgeHours;
        }

        public void SetHours(double _ageHours)
        {
            this.mAgeHours = _ageHours;
        }

        public double GetMinutes()
        {
            return mAgeMinutes;
        }

        public void SetMinutes(double _ageMinutes)
        {
            this.mAgeMinutes = _ageMinutes;
        }

        public double AgeConvertDays()
        {
            mAgeDays =(2016- mStartAge) * 365;

            return mAgeDays;
        }

        public double AgeConvertHours()
        {
            mAgeHours = mAgeDays * 24;

            return mAgeHours;
        }

        public double AgeConvertMinutes()
        {
            mAgeMinutes = mAgeHours * 60;

            return mAgeMinutes;
        }
    }
}
