﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taber_James_Portfolio
{
    class Backwards
    {

        public string mUserSent;

        public Backwards(string _userSent)
        {
            mUserSent = _userSent;
        }

        public string GetUserSent()
        {
            return mUserSent;
        }

        public void SetUserSent(string _userSent)
        {
            this.mUserSent = _userSent;
        }

        public string ReverseString()
        {
            char[] arr = this.mUserSent.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }
    }
}
