﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taber_James_Portfolio
{
    class Program
    {
        static void Main(string[] args)
        {
            bool done = false;

            while (!done)
            {
                //create the wording for the menu
                Console.WriteLine("Main Menu\r\n\r\n");
            Console.WriteLine("1. Swap Your Name");
            Console.WriteLine("2. Show a sentence you write backwards");
            Console.WriteLine("3. Convert you age into number of days, hours, minutes, and seconds");
            Console.WriteLine("4. Convert a temperature from Faharenhiet to Celsius and Celsius to Faharenhiet");
            Console.WriteLine("5. Exit\r\n\r\n");
            Console.WriteLine("Please enter the number to the option you wish to choose.");

            //capture and store what the user has entered
            string userInput = Console.ReadLine();

                //Create the conditionals for the users response
                //The first one is for swaping their name around
                if (userInput == "1")
                {
                    Console.WriteLine("What is your first name?");

                    string nameFirst = Console.ReadLine();

                    Console.WriteLine("What is your last name?");

                    string nameLast = Console.ReadLine();

                    Console.WriteLine("So your name is {0} {1}. Nice to meet you!",nameFirst, nameLast);

                    SwapName userName = new SwapName(nameFirst, nameLast);

                    Console.WriteLine(userName.NameSwap());


                }

                //this conditional is to reverse their sentence around
                else if (userInput == "2")
                {
                    //Ask the user for their sentence
                    Console.WriteLine("Please enter your sentence you would like to see be reversed");

                    //create a variable to capture the users sentence
                    string userSent1 = Console.ReadLine();

                    Backwards sentUser = new Backwards(userSent1);

                    Console.WriteLine(sentUser.ReverseString());

                }

                //this conditional is to convert their age
                else if (userInput == "3")
                {
                    Console.WriteLine("What year were you born in?");

                    string birthYear = Console.ReadLine();

                    double doubBirthYear;

                    while (!double.TryParse(birthYear, out doubBirthYear))
                    {
                        Console.WriteLine("I'm sorry I didn't get that. Will you please enter the year you were born?");

                        birthYear = Console.ReadLine();
                    }

                    AgeConvert ageChange = new AgeConvert(doubBirthYear);

                    double ageDays = ageChange.AgeConvertDays();

                    Console.WriteLine("You are " + ageDays + " days old!");

                    ageChange.SetDays(ageDays);

                    double ageHours = ageChange.AgeConvertHours();

                    Console.WriteLine("You are " + ageHours + " hours old!");

                    ageChange.SetHours(ageHours);

                    double ageMinutes = ageChange.AgeConvertMinutes();

                    Console.WriteLine("You are " + ageMinutes + " minutes old!");

                    double ageSeconds = ageMinutes * 60;

                    Console.WriteLine("You are " + ageSeconds + " seconds old!");
                                        
                }

                //this conditional is to convert temperature
                else if (userInput == "4")
                {
                    //ask the user what they are tryint to conver to
                    Console.WriteLine("What are you trying to convert the temperature to? Please enter Celsius or Faharenheit");

                    //capture the users response
                    string userInput2 = Console.ReadLine();

                    //set a conditional according to their response
                    if (userInput2.ToLowerInvariant() == "faharenheit")
                    {
                        //ask the user their starting temp
                        Console.WriteLine("What is the temperature in Celsius that you are trying to convert into Faharenheit?");

                        //declare a varible to convert the users response into
                        double userTempC;

                        //capture the users response
                        string stringUserTempC = Console.ReadLine();

                        //validate that the user has input a number while converting it
                        while (!double.TryParse(stringUserTempC, out userTempC))
                        {
                            Console.WriteLine("I'm sorry could you enter your starting temperature are a numeral?");

                            //re-capture the users input
                            stringUserTempC = Console.ReadLine();
                        }

                        TempConvert cToF = new TempConvert(0, userTempC);

                        Console.WriteLine("So your wanting to convert " + userTempC + " degrees celsius to Faharenheit.");

                        double tempF = cToF.TempConvertRun2();

                        Console.WriteLine("The converted temperature is " + tempF + " degrees faharenheit");

                       
                    }
                    else if (userInput2.ToLowerInvariant() == "celsius")
                    {
                        //ask the user their starting temp
                        Console.WriteLine("What is the temperature in Faharenheit that you are trying to convert into Celsius?");

                        //declare a varible to convert the users response into
                        double userTempF;

                        //capture the users response
                        string stringUserTempF = Console.ReadLine();

                        //validate that the user has input a number while converting it
                        while (!double.TryParse(stringUserTempF, out userTempF))
                        {
                            Console.WriteLine("I'm sorry could you enter your starting temperature are a numeral?");

                            //re-capture the users input
                            stringUserTempF = Console.ReadLine();
                        }

                        //Transfer the information into the custom class
                        TempConvert cToC = new TempConvert(userTempF, 0);

                       //confirm the information that has been entered
                        Console.WriteLine("So you entered "+userTempF+" degrees Faharenheit");

                        double tempC = cToC.TempConvertRun();

                        Console.WriteLine("The converted temperature is " + tempC + " degrees celsius");
                    }

                }

                //this one is to exit the application all together
                else if (userInput == "5")
                {
                    done = true;
                  //  Environment.Exit(0);

                }

            }

            Console.WriteLine("Thank you for using the program!");

      
    }
        public void ConverDays(double doubBirthYear) { }
    }
}
