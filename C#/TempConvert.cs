﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taber_James_Portfolio
{
    class TempConvert
    {
        //creat global variables for the class
        public double  mTempF;
        public double  mTempC;

        //create function and parameters for the member variables
        public TempConvert(double _tempF, double _tempC)
        {
            //set values for the member variables
            mTempF = _tempF;
            mTempC = _tempC;
        }
    
        //create a getter for the member variable
        public double GetTempF()
        {
            //return the value
            return mTempF;
        }

        //create a setter for the temperature in Fahrenheit
        public void SetTempF(double _tempF)
        {
            //change the value of the member variable
            this.mTempF = _tempF;
        }


        //now lets do the math to convert the temp from F to C by creating a method for it.
        public double TempConvertRun()
        {

            double tempC = (mTempF - 32) * 5 / 9;

            //return this value
            return tempC;
        }

        //create a getter 
        public double GetTempC()
        {
            return mTempC;
        }

        //creat a setter
        public void SetTempC(double _tempC)
        {
            this.mTempC = _tempC;
        }

        //now lets do the math to convert the temp from C to F by creating a method for it.
        public double TempConvertRun2()
        {

            double tempF = (mTempC * 1.8) + 32;

            return tempF;
        }

    }
}
    