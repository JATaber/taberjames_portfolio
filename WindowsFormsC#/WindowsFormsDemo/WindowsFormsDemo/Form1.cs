﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;


namespace WindowsFormsDemo
{
    /*
     * REFERENCES:
     * 
     * Icons were acquired from the site http://www.iconarchive.com/tag/download 
     * 
     * Json written with Newtonsoft JSON.net http://www.newtonsoft.com/json 
     * 
     * 
     * -Playstation Logo, Nintento Wii U Image, and Xbox Logo- 
     * http://icons8.com 
     *   
     * -Nintento Switch Logo- 
     * Nintendo of America, Inc. (n.d.). Nintento Switch Logo [This is the official logo for 
     * the Nintendo Switch console.]. Retrieved from 
     * https://commons.wikimedia.org/wiki/File:NintendoSwitchLogo.svg     
     */

    public partial class Form1 : Form
    {
        List<Game> games = new List<Game>();
                
        public Game Games
        {
            
            get
            {
                Game g = new Game();
                g.Title = titleText.Text;
                g.Cost = costNum.Value;
                g.Time = hrsPlayedNum.Value;
                g.Platform = platformCombo.Text;
                g.Complete = completeCheckBox.Checked;

                return g;
            }

            set
            {
                titleText.Text = value.Title;
                costNum.Value = value.Cost;
                hrsPlayedNum.Value = value.Time;
                platformCombo.Text = value.Platform;
                completeCheckBox.Checked = value.Complete;
            }
        }

        public Form1()
        {
            InitializeComponent();
                        
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            games = new List<Game>();

            collectionListView.Items.Clear();

            Games = new Game();
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //check to see if anything is in the listview
            if(collectionListView.Items.Count > 0)
            {
                //if something is in listview clear it
                collectionListView.Items.Clear();
            }
                    
            //use a try/catch in case the user tries to load in a collection that doesn't exist
            try
            {
                //reads the file created in the application from previous save
                string fileName = String.Format(@"{0}\collection.json", Application.StartupPath);

                //reads entire file and saves as a string
                string json = File.ReadAllText(fileName);
                                
                games = JsonConvert.DeserializeObject<List<Game>>(json);
            }
            catch (Exception)
            {
                MessageBox.Show("I'm sorry you haven't saved a collection to load in yet.");
            }

            //write out for testing purposes
            Console.WriteLine(games.Count);
            
            //helper method to add from the list to the listview
            AddToListView();

            //updates the listview with the current information
            collectionListView.Update();

            //displays the count of games in the collection
            gameCountLabel.Text = "Number of Games: " + games.Count;
        }            

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //saves the information within the application itself
            string fileName = String.Format(@"{0}\collection.json", Application.StartupPath);

            //serializes information into a string
            string json = JsonConvert.SerializeObject(games, Formatting.Indented);

            //writes the string into the fil
            File.WriteAllText(fileName, json);

            //this is for testing purposes to confirm it is proper json
            Console.WriteLine(json);

            //let the user know the information has been saved
            MessageBox.Show("Your information has been saved!");        
        }

       

        private void titleText_TextChanged(object sender, EventArgs e)
        {
            titleText.Text = Games.Title;
        }

        private void costNum_ValueChanged(object sender, EventArgs e)
        {
            costNum.Value = Games.Cost;
        }

        private void hrsPlayedNum_ValueChanged(object sender, EventArgs e)
        {
            hrsPlayedNum.Value = Games.Time;
        }

        private void platformCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            platformCombo.Text = Games.Platform;
        }

        private void completeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            completeCheckBox.Checked = Games.Complete;
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            //adds new game to the list
            games.Add(Games);

            //use helper method to add from the list to the listview
            AddToListView();
            
            //displays current information in the listview
            collectionListView.Update();

            //creates a new instance of Game to clear the entry fields
            Games = new Game();

            //checks to see if there are games in the list, if so displays the count to the user
            if (games.Count > 0)
            {
                gameCountLabel.Text = "Number of Games: " + games.Count;
            }

            //feedback to the user to let them know what has happened
            MessageBox.Show("The game has been added!");
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Games = new Game();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //this closes the application
            Application.Exit();
        }

        private void infoTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(infoTab.SelectedIndex == 1)
            {
                //this displays the count of games in the application
                totalTitlesText.Text = games.Count.ToString();

                //this shows how much the user has spent on games 
                totalCostText.Text = games.Sum(item => item.Cost).ToString();

                //this is how many hrs the user has played across all games
                totalHrsText.Text = games.Sum(item => item.Time).ToString();

                CountCompleted();                                
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            //makes sure the user has selected an item in the listview
            if (collectionListView.SelectedItems.Count > 0)
            {
                //create a listview item to repopulate the list
                ListViewItem selItem = collectionListView.SelectedItems[0];

                //stores the index of the selected 
                int itemIndex = collectionListView.Items.IndexOf(selItem);

                //removes the selected entry
                collectionListView.Items.RemoveAt(itemIndex);

                //clears the list
                games.Clear();

                //uses the helper method to repopulate the list
                ListViewToList();
                
                //this is to test to make sure the the list has been repopulated
                Console.WriteLine(games.Count);

                //resets the count of games to the current number if all the games haven't been deleted
                if (games.Count > 0)
                {
                    gameCountLabel.Text = "Number of Games: " + games.Count;
                }

                //if all the games have been deleted the get the label to display number of games to 0
                else if (games.Count <= 0)
                {
                    gameCountLabel.Text = "Number of Games: 0";
                }

                Console.WriteLine(games[0].Title);
            }
            else
            {
                MessageBox.Show("You must select an item to delete it.");
            }
            
            totalTitlesText.Text = games.Count.ToString();

            totalCostText.Text = games.Sum(item => item.Cost).ToString();

            totalHrsText.Text = games.Sum(item => item.Time).ToString();
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            ModifyForm dialog = new ModifyForm();
            dialog.ModifyGame += HandleModifyGame;

            if (collectionListView.SelectedItems.Count > 0)
            {
                dialog.gameInfo = collectionListView.SelectedItems[0].Tag as Game;

                dialog.SetVisible(true);

                dialog.ShowDialog();
            }
            
        }

        public void AddToListView()
        {
            collectionListView.Clear();

            //this is to add the items in the listview into the list by creating a listview item and storing the object to its tag property
            foreach (Game g in games)
            {
                ListViewItem item = new ListViewItem();

                item.Text = g.ToString();
                item.Tag = g;

                if (g.Platform == "Nintendo Switch")
                {
                    item.ImageIndex = 0;
                }
                else if (g.Platform == "Nintendo Wii U")
                {
                    item.ImageIndex = 1;
                }
                else if (g.Platform == "Steam")
                {
                    item.ImageIndex = 2;
                }
                else if (g.Platform == "Playstation 3" || g.Platform == "Playstation 4")
                {
                    item.ImageIndex = 3;
                }
                else if (g.Platform == "Xbox 360" || g.Platform == "Xbox One")
                {
                    item.ImageIndex = 4;
                }                           
                
                collectionListView.Items.Add(item);

                Console.WriteLine(g.Title);
            }
        }

        public void ListViewToList()
        {
            //This stores the object into the tag propery for the listview and stores it into the listview
            foreach(ListViewItem item in collectionListView.Items)
            {
                Game g = new Game();

                g = item.Tag as Game;

                games.Add(g);

                Console.WriteLine(g.Title);
            }
        }

        private void collectionListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ModifyForm dialog = new ModifyForm();
            
            if(collectionListView.SelectedItems.Count > 0)
            {
                dialog.gameInfo = collectionListView.SelectedItems[0].Tag as Game;

                dialog.ShowDialog();
            }
        }

        //this is a helper method to acquire the count of games that the user has completed
        public void CountCompleted()
        {
            //sets a variable to 0 in case the user hasn't completed any games yet
            int complete = 0;

            //a for loop to go through the objects in the list to check if the user has checked the completed property
            for (int i = 0; i< games.Count; i++)
            {
                //if the user has completed a game add one to the variable to display this information
                if(games[i].Complete == true)
                {
                    complete++;
                }
            }

            //displays the completed game count to the user
            totalComplete.Text = complete.ToString();
        }

        public void HandleModifyGame(object sender, ModifyForm.ModifyObjectEventArgs e)
        {
            ModifyForm d = sender as ModifyForm;

            Game g = e.GameModified1 as Game;

            ListViewItem item = new ListViewItem();
            ListViewItem selItem = collectionListView.SelectedItems[0];

            int itemIndex = collectionListView.Items.IndexOf(selItem);

            item.Text = g.ToString();
            item.Tag = g;

            if (g.Platform == "Nintendo Switch")
            {
                item.ImageIndex = 0;
            }
            else if (g.Platform == "Nintendo Wii U")
            {
                item.ImageIndex = 1;
            }
            else if (g.Platform == "Steam")
            {
                item.ImageIndex = 2;
            }
            else if (g.Platform == "Playstation 3" || g.Platform == "Playstation 4")
            {
                item.ImageIndex = 3;
            }
            else if (g.Platform == "Xbox 360" || g.Platform == "Xbox One")
            {
                item.ImageIndex = 4;
            }

            collectionListView.Items.RemoveAt(itemIndex);

            collectionListView.Items.Add(item);

            games.Clear();

            ListViewToList();

            CountCompleted();

            totalTitlesText.Text = games.Count.ToString();

            totalCostText.Text = games.Sum(item1 => item1.Cost).ToString();

            totalHrsText.Text = games.Sum(item1 => item1.Time).ToString();

            d.Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About dialog = new About();

            dialog.ShowDialog();
        }
    }
}
