﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsDemo
{
    public partial class ModifyForm : Form
    {

        public EventHandler<ModifyObjectEventArgs> ModifyGame;

        public void SetVisible(bool show)
        {
            editButton.Visible = show;
        }

        public Game gameInfo
        {
            get
            {
                Game g = new Game();
                g.Title = titleText.Text;
                g.Cost = costNum.Value;
                g.Time = hrsPlayedNum.Value;
                g.Platform = platformCombo.Text;
                g.Complete = completeCheckBox.Checked;

                return g;
            }
            set
            {
                titleText.Text = value.Title;
                costNum.Value = value.Cost;
                hrsPlayedNum.Value = value.Time;
                platformCombo.Text = value.Platform;
                completeCheckBox.Checked = value.Complete;
            }
        }

        public ModifyForm()
        {
            InitializeComponent();
        }

        private void titleText_TextChanged(object sender, EventArgs e)
        {
            titleText.Text = gameInfo.Title;
        }

        private void costNum_ValueChanged(object sender, EventArgs e)
        {
            costNum.Value = gameInfo.Cost;
        }

        private void hrsPlayedNum_ValueChanged(object sender, EventArgs e)
        {
            hrsPlayedNum.Value = gameInfo.Time;
        }

        private void platformCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            platformCombo.Text = gameInfo.Platform;
        }

        private void completeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            completeCheckBox.Checked = gameInfo.Complete;
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            ModifyGame?.Invoke(this, new ModifyObjectEventArgs(gameInfo));
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public class ModifyObjectEventArgs : EventArgs
        {
            Game GameModified;

            public Game GameModified1
            {
                get { return GameModified; }
                set { GameModified = value; }
            }

            public ModifyObjectEventArgs(Game ga)
            {
                GameModified = ga;
            }
        }
    }
}
