﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;


namespace WindowsFormsDemo
{   
    [DataContract]
    public class Game : Form1
    {
        [DataMember]
        string mTitle;
        [DataMember]
        decimal mCost;
        [DataMember]
        decimal mTime;
        [DataMember]
        string mPlatform;
        [DataMember]
        bool mComplete;

        public string Title
        {
            get { return mTitle; }
            set { mTitle = value; }
        }

        public decimal Cost
        {
            get { return mCost; }
            set { mCost = value; }
        }

        public decimal Time
        {
            get { return mTime; }
            set { mTime = value; }
        }

        public string Platform
        {
            get { return mPlatform; }
            set { mPlatform = value; }
        }

        public bool Complete
        {
            get { return mComplete; }
            set { mComplete = value; }
        }

        
        public override string ToString()
        {
            return mTitle;
        }
        
    }
}
