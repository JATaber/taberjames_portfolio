﻿namespace WindowsFormsDemo
{
    partial class ModifyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModifyForm));
            this.editButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.costNum = new System.Windows.Forms.NumericUpDown();
            this.titleText = new System.Windows.Forms.TextBox();
            this.hrsPlayedNum = new System.Windows.Forms.NumericUpDown();
            this.platformCombo = new System.Windows.Forms.ComboBox();
            this.completeCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.costNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hrsPlayedNum)).BeginInit();
            this.SuspendLayout();
            // 
            // editButton
            // 
            this.editButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.editButton.ForeColor = System.Drawing.Color.Black;
            this.editButton.Location = new System.Drawing.Point(8, 319);
            this.editButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(117, 36);
            this.editButton.TabIndex = 7;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Visible = false;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(90)))), ((int)(((byte)(105)))));
            this.groupBox1.Controls.Add(this.cancelButton);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.costNum);
            this.groupBox1.Controls.Add(this.titleText);
            this.groupBox1.Controls.Add(this.hrsPlayedNum);
            this.groupBox1.Controls.Add(this.platformCombo);
            this.groupBox1.Controls.Add(this.completeCheckBox);
            this.groupBox1.Controls.Add(this.editButton);
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(216)))), ((int)(((byte)(216)))));
            this.groupBox1.Location = new System.Drawing.Point(13, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(315, 365);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Entry to View/Modify";
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.cancelButton.ForeColor = System.Drawing.Color.Black;
            this.cancelButton.Location = new System.Drawing.Point(195, 319);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(112, 36);
            this.cancelButton.TabIndex = 19;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(216)))), ((int)(((byte)(216)))));
            this.label8.Location = new System.Drawing.Point(16, 212);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 20);
            this.label8.TabIndex = 18;
            this.label8.Text = "Games Platform";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(216)))), ((int)(((byte)(216)))));
            this.label7.Location = new System.Drawing.Point(16, 171);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 20);
            this.label7.TabIndex = 17;
            this.label7.Text = "Hours Played";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(216)))), ((int)(((byte)(216)))));
            this.label6.Location = new System.Drawing.Point(16, 131);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 20);
            this.label6.TabIndex = 16;
            this.label6.Text = "Game Cost";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(216)))), ((int)(((byte)(216)))));
            this.label5.Location = new System.Drawing.Point(16, 92);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 20);
            this.label5.TabIndex = 15;
            this.label5.Text = "Game Title";
            // 
            // costNum
            // 
            this.costNum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.costNum.BackColor = System.Drawing.Color.White;
            this.costNum.DecimalPlaces = 2;
            this.costNum.Location = new System.Drawing.Point(148, 129);
            this.costNum.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.costNum.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.costNum.Name = "costNum";
            this.costNum.Size = new System.Drawing.Size(158, 26);
            this.costNum.TabIndex = 11;
            this.costNum.ValueChanged += new System.EventHandler(this.costNum_ValueChanged);
            // 
            // titleText
            // 
            this.titleText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.titleText.BackColor = System.Drawing.Color.White;
            this.titleText.Location = new System.Drawing.Point(148, 89);
            this.titleText.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.titleText.Name = "titleText";
            this.titleText.Size = new System.Drawing.Size(158, 26);
            this.titleText.TabIndex = 10;
            this.titleText.TextChanged += new System.EventHandler(this.titleText_TextChanged);
            // 
            // hrsPlayedNum
            // 
            this.hrsPlayedNum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.hrsPlayedNum.BackColor = System.Drawing.Color.White;
            this.hrsPlayedNum.Location = new System.Drawing.Point(148, 169);
            this.hrsPlayedNum.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.hrsPlayedNum.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.hrsPlayedNum.Name = "hrsPlayedNum";
            this.hrsPlayedNum.Size = new System.Drawing.Size(158, 26);
            this.hrsPlayedNum.TabIndex = 12;
            this.hrsPlayedNum.ValueChanged += new System.EventHandler(this.hrsPlayedNum_ValueChanged);
            // 
            // platformCombo
            // 
            this.platformCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.platformCombo.BackColor = System.Drawing.Color.White;
            this.platformCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.platformCombo.FormattingEnabled = true;
            this.platformCombo.Items.AddRange(new object[] {
            "Nintendo Switch",
            "Nintendo Wii U",
            "PC",
            "Playstation 3",
            "Playstation 4",
            "Xbox 360",
            "Xbox One"});
            this.platformCombo.Location = new System.Drawing.Point(148, 209);
            this.platformCombo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.platformCombo.Name = "platformCombo";
            this.platformCombo.Size = new System.Drawing.Size(158, 28);
            this.platformCombo.TabIndex = 13;
            this.platformCombo.SelectedIndexChanged += new System.EventHandler(this.platformCombo_SelectedIndexChanged);
            // 
            // completeCheckBox
            // 
            this.completeCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.completeCheckBox.AutoSize = true;
            this.completeCheckBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(216)))), ((int)(((byte)(216)))));
            this.completeCheckBox.Location = new System.Drawing.Point(148, 247);
            this.completeCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.completeCheckBox.Name = "completeCheckBox";
            this.completeCheckBox.Size = new System.Drawing.Size(153, 24);
            this.completeCheckBox.TabIndex = 14;
            this.completeCheckBox.Text = "Game Completed";
            this.completeCheckBox.UseVisualStyleBackColor = true;
            this.completeCheckBox.CheckedChanged += new System.EventHandler(this.completeCheckBox_CheckedChanged);
            // 
            // ModifyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(37)))), ((int)(((byte)(46)))));
            this.ClientSize = new System.Drawing.Size(341, 393);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ModifyForm";
            this.Text = "Modify";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.costNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hrsPlayedNum)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown costNum;
        private System.Windows.Forms.TextBox titleText;
        private System.Windows.Forms.NumericUpDown hrsPlayedNum;
        private System.Windows.Forms.ComboBox platformCombo;
        private System.Windows.Forms.CheckBox completeCheckBox;
        private System.Windows.Forms.Button cancelButton;
    }
}